package com.csvikram.springdatajpaencryption;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author vikram.choudhary 27/05/20
 */

public interface CardDetailRepository extends JpaRepository<CardDetail, Long> {}
