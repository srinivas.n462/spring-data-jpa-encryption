package com.csvikram.springdatajpaencryption;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author vikram.choudhary 27/05/20
 */

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CardDetail {

	@Id
	@GeneratedValue
	private Long id;

	private String name;

	@Convert(converter = StringAttributeConverter.class)
	private String cardNumber;
	private String expiry;

}
